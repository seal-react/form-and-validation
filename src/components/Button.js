import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';

import Loading from './Loading';
import {colors, dimens, fonts} from '../utils';

const Button = ({
  onPress,
  isDisabled,
  title,
  customButtonStyle,
  customTitleStyle,
  isLoading,
  isOutlined,
  LeftIcon,
  RightIcon,
  customLeftIconStyle,
  customRightIconStyle,
  borderColor = colors.primary,
  textColor = colors.white,
  backgroundColor = colors.primary,
}) => {
  return (
    <TouchableOpacity
      disabled={isDisabled}
      onPress={isLoading ? null : () => onPress()}
      style={[
        styles.container,
        {backgroundColor},
        isOutlined ? styles.outlined : {},
        isOutlined ? {borderColor} : {},
        isDisabled ? {backgroundColor: colors.gray4} : {},
        customButtonStyle,
      ]}
      // activeOpacity={1}
    >
      {LeftIcon && <LeftIcon style={[styles.icon, {customLeftIconStyle}]} />}
      {isLoading ? (
        <View style={styles.loading}>
          <Loading color={textColor} />
        </View>
      ) : (
        <Text
          allowFontScaling={false}
          style={[styles.title, {color: textColor}, customTitleStyle]}>
          {title || 'Button'}
        </Text>
      )}
      {RightIcon && <RightIcon style={[styles.icon, {customRightIconStyle}]} />}
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: {
    height: dimens[46],
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: dimens[6],
  },
  loading: {
    marginHorizontal: dimens[12],
  },
  outlined: {
    borderWidth: 1,
    backgroundColor: 'transparent',
  },
  title: {
    fontFamily: fonts.semibold,
    fontSize: dimens[14],
    marginHorizontal: dimens[12],
  },
  icon: {
    height: dimens[40],
    width: dimens[40],
  },
});
