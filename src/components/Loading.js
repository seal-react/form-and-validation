import React from 'react';
import {ActivityIndicator} from 'react-native';

const Loading = ({color, size}) => {
  return <ActivityIndicator color={color || 'black'} size={size || 'small'} />;
};

export default Loading;
